from django.conf.urls import url
from django.conf import settings
from django.views.static import serve
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import refresh_jwt_token
from storyapi import views

urlpatterns = [
    url(r'^storyapi/storys$', views.story_list.as_view()),
    url(r'^storyapi/user', views.users.as_view()),
    url(r'^storyapi/signin', obtain_jwt_token),
    url(r'^storyapi/refresh', refresh_jwt_token),
    url(r'^storyapi/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT,}),
]

urlpatterns = format_suffix_patterns(urlpatterns)
