# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from django.db import models
from django.contrib.auth.models import User

def create_folder(path):
    if not os.path.exists("/home/pi/Projects/storyServer/"+path):
        os.makedirs("/home/pi/Projects/storyServer/"+path)


def audio_upload_to(instance,filename):
    username = ""
    if isinstance(instance,Resources):
        username = instance.sid.uid.username
    else:
        username = instance.uid.username
    return "resources/audio/storyresource/"+username+"/"+filename
def image_upload_to(instance,filename):
    username = ""
    if isinstance(instance,Resources):
        username = instance.sid.uid.username
    else:
        username = instance.uid.username
    return "resources/image/storyresource/"+username+"/"+filename

# Create your models here
class Story(models.Model):
    uid = models.ForeignKey(User,on_delete=models.CASCADE)
    lat = models.FloatField()
    lng = models.FloatField()
    title = models.CharField(max_length=32,unique=True)
    description = models.CharField(max_length=272)
    img = models.FileField(upload_to=image_upload_to)

class Resources(models.Model):
    sid = models.ForeignKey(Story, on_delete=models.CASCADE)
    place = models.IntegerField()
    lat = models.FloatField(null=True)
    lng = models.FloatField(null=True)
    img = models.FileField(upload_to=image_upload_to,null=True)
    audio = models.FileField(upload_to=audio_upload_to,null=True)
