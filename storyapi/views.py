# -*- coding: utf-8 -*-
from rest_framework import status
from rest_framework import generics
from rest_framework import permissions
from django.contrib.auth.models import User
from storyapi.permissions import IsOwnerOrReadOnly, IsOwnerOrAny
from rest_framework.response import Response
from rest_framework.views import APIView
from storyapi.models import Story
from storyapi.serializers import StorySerializer, UserSerializer

class story_list(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly,)
    queryset = Story.objects.all()
    serializer_class = StorySerializer

    def perform_create(self, serializer):
        serializer.save(uid=self.request.user)

class users(generics.ListCreateAPIView):
    permissions_classes = (IsOwnerOrAny,)
    serializer_class = UserSerializer

    def list(self, request):
        queryset = User.objects.get(id=request.user.id)
        serializer = UserSerializer(queryset, many=False)
        return Response(serializer.data)
