from rest_framework import serializers
from storyapi.models import Story
from django.contrib.auth.models import User

class StorySerializer(serializers.ModelSerializer):
    uid = serializers.ReadOnlyField(source='uid.username')
    class Meta:
        model = Story
        fields = ('id','uid','lat','lng','title','description','img')

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','username','email','password','first_name','last_name')
        write_only_fields = ('password',)
        read_only_fields = ('id',)

    def create(self, data):
        user = User.objects.create(
            username = data['username'],
            email = data['email'],
            first_name = data['first_name'],
            last_name = data['last_name']
        )
        user.set_password(data['password'])
        user.save()
        return user
