# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-05-29 05:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('storyapi', '0003_auto_20180526_0345'),
    ]

    operations = [
        migrations.AddField(
            model_name='story',
            name='description',
            field=models.CharField(default='Test', max_length=272),
            preserve_default=False,
        ),
    ]
